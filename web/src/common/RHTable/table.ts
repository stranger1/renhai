import * as request from "@/axios/axios";

/**
 * 获取候选参数
 * @param url
 * @param param
 * @param method
 */
export async function getVlues(
  url: string,
  param: {},
  method: String = "get"
) {
  return await request.requestAssign(url, param, method);
}
