import * as request from "@/axios/axios";

let _ = require("lodash");

interface input {
  message: String; //提示信息
  type: String; //类型
  required: boolean; //是否允许null
  min: Number; //最小值
  max: Number; //最大值
}

/**
 * 文本框
 * @param param
 * @param module
 */
export function input(param: input, module: String): Array<{}> {
  let prop = [];

  // 数字格式
  if (param.type.toLocaleLowerCase() === "number") {
    prop.push({ type: "number", message: module + "必须为数字值" });
    if (!_.isNil(param.min) && !_.isNil(param.max)) {
      if (param.max >= param.min) {
        prop.push({
          min: param.min,
          max: param.max,
          message: param.min + " ~ " + param.max + " 之间",
          trigger: "blur"
        });
      }
    } else if (!_.isNil(param.min)) {
      prop.push({
        min: param.min,
        message: "最小为 " + param.min,
        trigger: "blur"
      });
    } else if (!_.isNil(param.max)) {
      prop.push({
        max: param.max,
        message: "最大为 " + param.max,
        trigger: "blur"
      });
    }

    /*if (param.required) {
      //非空
      prop.push({ required: true, message: "请输入" + module, trigger: "blur" });
    }*/

  }else{
    //长度
    if (!_.isNil(param.min) && !_.isNil(param.max)) {
      if (param.max >= param.min) {
        prop.push({
          min: param.min,
          max: param.max,
          message: "长度在 " + param.min + " 到 " + param.max + " 个字符",
          trigger: "blur"
        });
      }
    } else if (!_.isNil(param.min)) {
      prop.push({
        min: param.min,
        message: "最少输入 " + param.min + " 个字符",
        trigger: "blur"
      });
    } else if (!_.isNil(param.max)) {
      prop.push({
        max: param.max,
        message: "最多输入 " + param.max + " 个字符",
        trigger: "blur"
      });
    }
  }
  return prop;
}

interface select {
  message: String; //提示信息
  type: String; //类型
  required: boolean; //是否为null
}

/**
 * 下拉框
 * @param param
 * @param module
 */
export function select(param: select, module: String): Array<{}> {
  let prop = [];
  if (param.required) {
    //非空
    if (_.isEmpty(param.type)) {
      prop.push({
        required: true,
        message: "请选择" + module,
        trigger: "change"
      });
    } else {
      prop.push({
        type: param.type,
        required: true,
        message: "请选择" + module,
        trigger: "change"
      });
    }
  }

  return prop;
}

interface checkbox {
  message: String;
  type: String;
  required: boolean;
}

/**
 * 多选框
 * @param param
 * @param module
 */
export function checkbox(param: checkbox, module: String): Array<{}> {
  let prop = [];
  if (param.required) {
    //非空
    prop.push({
      type: "array",
      required: true,
      message: "请至少选择一个" + module,
      trigger: "change"
    });
  }
  return prop;
}

interface radio {
  message: String;
  type: String;
  required: boolean;
}

/**
 * 单选
 * @param param
 * @param module
 */
export function radio(param: radio, module: String): Array<{}> {
  let prop = [];
  if (param.required) {
    //非空
    prop.push({
      required: true,
      message: "请选择" + module,
      trigger: "change"
    });
  }
  return prop;
}

interface date {
  message: String;
  type: String;
  required: boolean;
}

/**
 * 时间
 * @param param
 * @param module
 */
export function date(param: date, module: String): Array<{}> {
  let prop = [];
  if (param.required) {
    //非空
    prop.push({
      required: true,
      message: "请选择" + module,
      trigger: "change"
    });
  }
  return prop;
}

/**
 * 获取候选参数
 * @param url
 * @param param
 * @param method
 */
export async function getCandidateValues(
  url: string,
  param: {},
  method: String = "post"
) {
  return await request.requestAssign(url, param, method);
}
