//登陆
import * as request from "@/axios/axios";
import router from "@/router";
import store from "@/store";

let _ = require("lodash");

/**
 * 登陆
 * @param param
 */
export function loginUser(param: {}) {
  return request.post(
    store.state.url.user + "api/users/user/login.action",
    param
  );
}

interface resUtil {
  data: [any];
  entity: any;
  state: number;
  msg: string;
  code: number;
}

/**
 * 获取路由
 * @param param
 */
export function path(param: {}) {
  return new Promise(function(resolve, reject) {
    //生成菜单
    request
      .post(
        store.state.url.user + "api/users/renHaiModule/getMenu.action",
        param
      )
      .then((response: unknown) => {
        let res = response as resUtil;
        const data: any = res.data;
        let children: any = [];
        getChildren(data, children);
        const routers = [
          {
            path: store.state.menuShow === 1 ? "/首页" : "index",
            name: "首页",
            component: () => import("@/views/main/index.vue"), // 路由懒加载写法
            children: children
          }
        ];
        store.state.routerLength = routers.length;
        // @ts-ignore
        store.state.router = routers;
        // 添加路由
        let children2: any = [];
        getChildren2(data, children2, "");
        const routers2 = [
          {
            path: store.state.menuShow === 1 ? "/首页" : "index",
            name: "首页",
            component: () => import("@/views/main/index.vue"), // 路由懒加载写法
            children: children2
          }
        ];
        console.log(routers2);
        router.addRoutes(routers2);
        resolve();
      });
  });
}

function getChildren(data: [any], children: [any]) {
  if (data != null && data.length > 0) {
    for (let i = 0; i < data.length; i++) {
      if (store.state.menuShow === 1) {
        children.push({
          path: data[i]["moduleName"],
          name: data[i]["moduleName"],
          moduleGrade: data[i]["moduleGrade"],
          children: []
        });
      } else if (store.state.menuShow === 2) {
        children.push({
          path: data[i]["moduleUrl"],
          moduleGrade: data[i]["moduleGrade"],
          name: data[i]["moduleName"],
          children: []
        });
      }
      if (data[i] && data[i].children && data[i].children.length > 0) {
        getChildren(data[i].children, children[i].children);
      }
    }
  }
}

function getChildren2(data: [any], children: [any], path: string) {
  if (data != null && data.length > 0) {
    let temp = path;
    for (let i = 0; i < data.length; i++) {
      if (!_.isEmpty(data[i]["moduleUrl"])) {
        let tempUrl: string = data[i]["moduleUrl"];
        if (store.state.menuShow === 1) {
          children.push({
            path: path + "/" + data[i]["moduleName"],
            name: path + "/" + data[i]["moduleName"],
            component: () => import("@/" + tempUrl)
          });
        } else if (store.state.menuShow === 2) {
          children.push({
            path: path + "/" + data[i]["moduleUrl"],
            name: path + "/" + data[i]["moduleName"],
            component: () => import("@/" + tempUrl)
          });
        }
      }
      if (data[i] && data[i].children && data[i].children.length > 0) {
        if(store.state.menuShow === 1){
          if (path == "") {
            path = data[i]["moduleName"];
          } else {
            path += "/" + data[i]["moduleName"];
          }
        }else if (store.state.menuShow === 2){
          if (path == "") {
            path = data[i]["moduleUrl"];
          } else {
            path += "/" + data[i]["moduleName"];
          }
        }
        getChildren2(data[i].children, children, path);
      }
      path = temp;
    }
  }
}
