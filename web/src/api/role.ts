import * as request from "@/axios/axios";
import store from "@/store";

export function saveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiRole/saveUpdate",
    param
  );
}

export function del(param: {}) {
  return request.del(
    store.state.url.user + "/api/users/renHaiRole/delete",
    param
  );
}

export function details(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiRole/getDetails",
    param
  );
}
