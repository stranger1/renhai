const webpack = require("webpack");
module.exports = {
  devServer: {
    port: 8081, //端口号
    open: true, //启动打开浏览器
    disableHostCheck: true,
    sockHost: "wangdao.xiaomy.net",
    //配置跨区问题
    proxy: {
      "/api": {
        target: "http://127.0.0.1:8910/renHai", //API服务器的地址
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      }
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "windows.jQuery": "jquery"
      })
    ]
  }
};
