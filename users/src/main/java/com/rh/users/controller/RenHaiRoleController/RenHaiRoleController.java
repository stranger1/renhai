package com.rh.users.controller.RenHaiRoleController;


import com.rh.entity.role.RenHaiRole;
import com.rh.users.service.RenHaiRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;
import renhai.util.Constant;
import renhai.util.Page;
import renhai.util.ResponseContext;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Api("角色")
@RestController
@RequestMapping("/api/users/renHaiRole")
public class RenHaiRoleController {

    private final RenHaiRoleService service;

    public RenHaiRoleController(RenHaiRoleService service) {
        this.service = service;
    }

    /**
     * @return 执行结果
     */
    @ApiOperation("新增/修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "role", value = "角色信息", required = true)
    })
    @PostMapping("saveUpdate")
    public ResponseContext<String> saveUpdate(@RequestBody RenHaiRole role) {
        if (role != null) {
            //添加
            if (!StringUtils.isNotEmpty(role.getRoleId())) {
                if (service.save(role, role.getModuleIds())) {
                    return Constant.SAVE_S;
                }
            } else {
                if (service.update(role, role.getModuleIds())) {
                    return Constant.UPDATE_S;
                }
            }
        }
        return Constant.ERROR;
    }

    ;

    @PostMapping("getRoleList")
    @ApiOperation("角色分页列表")
    public ResponseContext getRoleList(@RequestBody Page<RenHaiRole> pageInfo) {
        return new ResponseContext<>(service.list(pageInfo, pageInfo.getParams()), 1);
    }

    @PostMapping("getRoleAllList")
    @ApiOperation("所有角色")
    public ResponseContext getRoleAllList() {
        return new ResponseContext<>(service.list());
    }

    @ApiOperation("角色详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleId", value = "角色id", required = true)
    })
    @PostMapping("getDetails")
    public ResponseContext getDetails(@RequestBody RenHaiRole role) {

        return new ResponseContext<>(service.getDetails(role.getRoleId()));
    }

    @ApiOperation("删除角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleId", value = "角色id", required = true)
    })
    @DeleteMapping("delete")
    public ResponseContext delete(@RequestBody RenHaiRole role) {
        if (service.deleteById(role.getRoleId())) {
            return Constant.DELETE_S;
        }
        return Constant.DELETE_E;
    }

}

