package com.rh.users.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rh.entity.user.RenHaiUser;
import com.rh.users.mapper.RenHaiUserMapper;
import com.rh.users.service.RenHaiUserRoleService;
import com.rh.users.service.RenHaiUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import renhai.util.Constant;
import renhai.util.Page;
import renhai.util.encrypt.PBEUtil;
import renhai.util.error.BusinessException;

/**
 * @author 57556
 */
@Service
public class RenHaiUserServiceImpl extends ServiceImpl<RenHaiUserMapper, RenHaiUser> implements RenHaiUserService {

    private final RenHaiUserMapper userMapper;
    private final RenHaiUserRoleService renHaiUserRoleService;

    public RenHaiUserServiceImpl(RenHaiUserMapper userMapper, RenHaiUserRoleService renHaiUserRoleService) {
        this.userMapper = userMapper;
        this.renHaiUserRoleService = renHaiUserRoleService;
    }

    @Override
    public RenHaiUser login(RenHaiUser users) {
        RenHaiUser user = userMapper.selectUserName(users.getUserName());
        if (user != null) {
            String pssword = PBEUtil.decrypt(user.getPassword(), user.getUserId(), PBEUtil.varIntToByteArray(Long.parseLong(user.getSalt())));
            //比较密码是否正确
            if (users.getPassword().equals(pssword)) {
                return user;
            }
            ;
        }
        return null;
    }

    @Override
    public PageInfo list(Page<RenHaiUser> page, RenHaiUser user) {
        QueryWrapper<RenHaiUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                //姓名
                .like(StringUtils.isNotBlank(user.getName()), RenHaiUser::getName, user.getName())
                //用户名
                .like(StringUtils.isNotBlank(user.getUserName()), RenHaiUser::getUserName, user.getUserName())
                .orderByDesc(RenHaiUser::getCreateTime);
        PageHelper.startPage(page);
        return new PageInfo<>(userMapper.selectList(queryWrapper));
    }

    @Override
    public boolean save(RenHaiUser user, String[] roleIds) {
        //添加角色
        if (this.save(user)) {
            //添加中间表 关联模块表 多对多
            if (!renHaiUserRoleService.saveBatch(roleIds, user.getUserId())) {
                throw new BusinessException(2, "角色添加失败");
            }
            return updatePassword(user.getUserId(), user.getPasswordEncrypt(), user.getSalt());
        }
        return false;
    }

    @Override
    public boolean deleteById(String id) {
        // 删除角色
        if (!renHaiUserRoleService.delete(id, null)) {
            throw new BusinessException(2, "角色删除失败");
        }
        return this.removeById(id);
    }

    @Override
    public boolean update(RenHaiUser user, String[] roleIds) {
        if (user != null) {
            RenHaiUser oldUser = this.getDetails(user.getUserId());

            UpdateWrapper<RenHaiUser> updateWrapper = new UpdateWrapper<>();
            updateWrapper.lambda()
                    .set(true, RenHaiUser::getUserName, user.getUserName())
                    .set(true, RenHaiUser::getName, user.getName())
                    .eq(true, RenHaiUser::getUserId, user.getUserId());
            if (userMapper.update(user, updateWrapper) > 0) {
                //删除用户的全部角色
                if (!renHaiUserRoleService.delete(user.getUserId(), null)) {
                    throw Constant.getBusinessException("角色删除失败");
                }
                //添加用户角色
                if (!renHaiUserRoleService.saveBatch(roleIds, user.getUserId())) {
                    throw Constant.getBusinessException("角色添加失败");
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public RenHaiUser getDetails(String id) {
        RenHaiUser renHaiUser = this.getById(id);

        renHaiUser.setRenHaiRoleList(renHaiUserRoleService.getList(id, null));
        return renHaiUser;
    }

    @Override
    public Integer getUserNameCount(String userName, String userId) {

        return userMapper.getUserNameCount(userName, userId);
    }

    @Override
    public boolean updatePassword(String userId, String password, String salt) {
        UpdateWrapper<RenHaiUser> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda()
                .set(true, RenHaiUser::getPassword, password)
                .set(true, RenHaiUser::getSalt, salt)
                .eq(true, RenHaiUser::getUserId, userId);
        if (StringUtils.isNotBlank(userId) && StringUtils.isNotBlank(password) && StringUtils.isNotBlank(salt)) {
            return userMapper.update(null, updateWrapper) > 0;
        }
        return false;
    }
}
