package com.rh.users.service.impl;

import com.rh.entity.user.RenHaiUserFile;
import com.rh.users.mapper.RenHaiUserFileMapper;
import com.rh.users.service.RenHaiUserFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-04-29
 */
@Service
public class RenHaiUserFileServiceImpl extends ServiceImpl<RenHaiUserFileMapper, RenHaiUserFile> implements RenHaiUserFileService {

   private final RenHaiUserFileMapper renHaiUserFileMapper;

   private final RestTemplateUtil restTemplateUtil;

   public  RenHaiUserFileServiceImpl(RenHaiUserFileMapper renHaiUserFileMapper, RestTemplateUtil restTemplateUtil) {
     this.renHaiUserFileMapper = renHaiUserFileMapper;
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<RenHaiUserFile> list(Page page) {
     QueryWrapper<RenHaiUserFile> queryWrapper = new QueryWrapper<>();
     PageHelper.startPage(page);
     return new PageInfo<>(renHaiUserFileMapper.selectList(queryWrapper));
   }
}
