package com.rh.users.service.impl;

import com.rh.entity.users.RenHaiTest;
import com.rh.users.mapper.RenHaiTestMapper;
import com.rh.users.service.RenHaiTestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-05-21
 */
@Service
public class RenHaiTestServiceImpl extends ServiceImpl<RenHaiTestMapper, RenHaiTest> implements RenHaiTestService {

   private final RenHaiTestMapper renHaiTestMapper;

   private final RestTemplateUtil restTemplateUtil;

   public  RenHaiTestServiceImpl(RenHaiTestMapper renHaiTestMapper, RestTemplateUtil restTemplateUtil) {
     this.renHaiTestMapper = renHaiTestMapper;
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<RenHaiTest> list(Page<RenHaiTest> page) {
     QueryWrapper<RenHaiTest> queryWrapper = new QueryWrapper<>();
     PageHelper.startPage(page);
     return new PageInfo<>(renHaiTestMapper.selectList(queryWrapper));
   }
}
