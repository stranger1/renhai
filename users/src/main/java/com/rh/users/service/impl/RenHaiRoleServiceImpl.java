package com.rh.users.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rh.entity.role.RenHaiRole;
import com.rh.users.RedisCache.MybatisRedisCache;
import com.rh.users.mapper.RenHaiRoleMapper;
import com.rh.users.service.RenHaiRoleModuleService;
import com.rh.users.service.RenHaiRoleService;
import org.apache.ibatis.annotations.CacheNamespace;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import renhai.util.Page;
import renhai.util.error.BusinessException;

import java.util.List;


/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Service
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public class RenHaiRoleServiceImpl extends ServiceImpl<RenHaiRoleMapper, RenHaiRole> implements RenHaiRoleService {

    private final RenHaiRoleMapper mapper;

    private final RenHaiRoleModuleService roleModuleService;

    public RenHaiRoleServiceImpl(RenHaiRoleMapper mapper, RenHaiRoleModuleService roleModuleService) {
        this.mapper = mapper;
        this.roleModuleService = roleModuleService;
    }

    /**
     * @param role      角色信息
     * @param moduleIds 角色拥有的模块id
     * @return true
     */
    @Override
    @Transactional
    public boolean save(RenHaiRole role, String[] moduleIds) {
        //添加角色
        if (this.save(role)) {
            //添加中间表 关联模块表 多对多
            if (!roleModuleService.saveBatch(moduleIds, role.getRoleId())) {
                throw new BusinessException(2, "模块添加失败");
            }
            return true;
        }
        return false;
    }

    /**
     * @param role      角色信息
     * @param moduleIds 角色拥有的模块id
     * @return true/false
     */
    @Override
    public boolean update(RenHaiRole role, String[] moduleIds) {

        if (role != null) {
            UpdateWrapper<RenHaiRole> updateWrapper = new UpdateWrapper<>();
            updateWrapper.lambda()
                    .set(true, RenHaiRole::getRoleName, role.getRoleName())
                    .set(true, RenHaiRole::getRoleType, role.getRoleType())
                    .eq(true, RenHaiRole::getRoleId, role.getRoleId());
            if (mapper.update(role, updateWrapper) > 0) {
                //删除角色的全部模块
                roleModuleService.delete(role.getRoleId(), null);
                //添加角色模块
                roleModuleService.saveBatch(moduleIds, role.getRoleId());
                return true;
            }
        }
        return false;
    }

    @Override
    public PageInfo list(Page page, RenHaiRole role) {
        QueryWrapper<RenHaiRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(RenHaiRole::getCreateTime);
        PageHelper.startPage(page);
        List<RenHaiRole> list = mapper.selectList(queryWrapper);
        return new PageInfo<>(list);
    }

    @Override
    public RenHaiRole getDetails(String id) {
        //获取角色详情
        RenHaiRole role = this.getById(id);
        role.setRenHaiRoleModuleList(roleModuleService.getList(id, null));
        return role;
    }

    @Override
    public boolean deleteById(String id) {
        //删除角色的全部模块
        if (!roleModuleService.delete(id, null)) {
            throw new BusinessException(2, "模块删除失败");
        }
        return this.removeById(id);
    }
}
