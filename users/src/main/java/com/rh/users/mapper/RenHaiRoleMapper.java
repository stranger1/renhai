package com.rh.users.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rh.entity.role.RenHaiRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Mapper
public interface RenHaiRoleMapper extends BaseMapper<RenHaiRole> {

}
