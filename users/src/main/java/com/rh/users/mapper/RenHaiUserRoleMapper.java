package com.rh.users.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rh.entity.user.RenHaiUserRole;
import com.rh.users.RedisCache.MybatisRedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Mapper
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public interface RenHaiUserRoleMapper extends BaseMapper<RenHaiUserRole> {

}
