package com.rh.users.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rh.entity.user.RenHaiUser;
import com.rh.users.RedisCache.MybatisRedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 57556
 */
@Mapper
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public interface RenHaiUserMapper extends BaseMapper<RenHaiUser> {
    /**
     * @param user
     * @return
     */
    List<RenHaiUser> getUserList(RenHaiUser user);

    /**
     * 查询用户名是否重复
     *
     * @param userName 用户名
     * @param userId   用户id
     * @return
     */
    Integer getUserNameCount(@Param("userName") String userName, @Param("userId") String userId);

    /**
     * 获取密码的盐
     *
     * @param userId
     * @return
     */
    String getSaltById(String userId);

    RenHaiUser selectUserName(String name);

}
