package com.rh.users.mapper;

import com.rh.entity.users.RenHaiCommonModule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* <p>
    * 公共组件  Mapper 接口
    * </p>
*
* @author wangdao
* @since 2020-07-22
*/
@Mapper
@Repository
public interface RenHaiCommonModuleMapper extends BaseMapper<RenHaiCommonModule> {

}
