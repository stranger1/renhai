package com.rh.users.mapper;

import com.rh.entity.user.RenHaiUserFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* <p>
    *  Mapper 接口
    * </p>
*
* @author wangdao
* @since 2020-04-29
*/
@Mapper
@Repository
public interface RenHaiUserFileMapper extends BaseMapper<RenHaiUserFile> {

}
