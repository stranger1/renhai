

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.FileType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aqian666
 * @ClassName: CodeGeneration
 * @Description: 代码生成器
 * @date 2019年7月2日14:08:37
 */
public class MybatisPlusGenerator {
    public static final String DB_URL = "jdbc:mysql://106.53.250.134:3306/renhai?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&useSSL=false";
    public static final String USER_NAME = "root";
    public static final String PASSWORD = "liLI980609@";
    public static final String DRIVER = "com.mysql.cj.jdbc.Driver";

    public static final String ENTITY = "/entity/src/main/java/com/rh/entity/";
    public static final String SE = "/users/";
    public static final String SERVICE = SE + "/src/main/java/com/rh" + SE;
    public static final String MAPPER = SE + "/src/main/java/com/rh" + SE;
    public static final String CONTROLLER = SE + "/src/main/java/com/rh" + SE;
    public static final String MAPPERXML = SE + "/src/main/resources/mapper/";


    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDriverName(DRIVER);
        dsc.setUsername(USER_NAME);
        dsc.setPassword(PASSWORD);
        dsc.setUrl(DB_URL);
        mpg.setDataSource(dsc);

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath);
        gc.setAuthor("wangdao");//作者名称
        gc.setSwagger2(true);
        gc.setOpen(false);
        gc.setIdType(IdType.ID_WORKER);
        gc.setDateType(DateType.ONLY_DATE);
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setControllerName("%sController");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setMapperName("%sMapper");
        mpg.setGlobalConfig(gc);

        //gc.setSwagger2(true); //实体属性 Swagger2 注解
        gc.setFileOverride(true);
        gc.setActiveRecord(false);// 不需要ActiveRecord特性的请改为false
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(false);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(null);
        pc.setMapper("com.rh.users.mapper");//dao
        pc.setService("com.rh.users.service");//servcie
        pc.setServiceImpl("com.rh.users.service.impl");
        pc.setController("com.rh.users.controller");//controller
        pc.setEntity("com.rh.entity.users");


        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
            }
        };

        // 模板引擎是 freemarker
        // 自定义controller的代码模板
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";
        String templatePath = "";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();

        // 自定义配置会被优先输出,配置mapper.xml
        FileOutConfig mapperXml = new FileOutConfig("/templates/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {

                // 自定义输入文件名称
                if (StringUtils.isEmpty(pc.getModuleName())) {
                    return projectPath + MybatisPlusGenerator.MAPPERXML + tableInfo.getXmlName() + StringPool.DOT_XML;
                } else {
                    return projectPath + MybatisPlusGenerator.MAPPERXML + pc.getModuleName() + "/" + tableInfo.getXmlName() + StringPool.DOT_XML;
                }
            }
        };


        //控制层
        templatePath = "/templates/controller.java.ftl";
        // 自定义配置会被优先输出
        FileOutConfig controller = new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 + pc.getModuleName()
                String expand = projectPath + MybatisPlusGenerator.CONTROLLER + "controller";
                return String.format((expand + File.separator + "%s" + ".java"), tableInfo.getControllerName());
            }
        };

        //业务层
        templatePath = "/templates/service.java.ftl";
        // 自定义配置会被优先输出
        FileOutConfig service = new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 + pc.getModuleName()
                String expand = projectPath + MybatisPlusGenerator.SERVICE + "service";
                return String.format((expand + File.separator + "%s" + ".java"), tableInfo.getServiceName());
            }
        };

        templatePath = "/templates/serviceImpl.java.ftl";
        // 自定义配置会被优先输出
        FileOutConfig serviceImpl = new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 + pc.getModuleName()
                String expand = projectPath + MybatisPlusGenerator.SERVICE + "service/impl";
                return String.format((expand + File.separator + "%s" + ".java"), tableInfo.getServiceImplName());
            }
        };

        //数据层
        templatePath = "/templates/mapper.java.ftl";
        // 自定义配置会被优先输出
        FileOutConfig mapper = new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 + pc.getModuleName()
                String expand = projectPath + MybatisPlusGenerator.MAPPER + "mapper";
                return String.format((expand + File.separator + "%s" + ".java"), tableInfo.getMapperName());
            }
        };

        //数据层
        templatePath = "/templates/entity.java.ftl";
        // 自定义配置会被优先输出
        FileOutConfig entity = new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 + pc.getModuleName()
                String expand = projectPath + MybatisPlusGenerator.ENTITY + MybatisPlusGenerator.SE;
                return String.format((expand + File.separator + "%s" + ".java"), tableInfo.getEntityName());
            }
        };

        cfg.setFileCreate(new IFileCreate() {
            /**
             * 自定义判断是否创建文件
             *
             * @param configBuilder 配置构建器
             * @param fileType      文件类型
             * @param filePath      文件路径
             * @return ignore
             */
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir(filePath);
                return true;
            }
        });
        //生成模板

        focList.add(mapper);
        focList.add(mapperXml);
        focList.add(service);
        focList.add(serviceImpl);
        focList.add(controller);
        focList.add(entity);

        mpg.setPackageInfo(pc);
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
//        templateConfig.setEntity("templates/1.java");
        // templateConfig.setService();
        templateConfig.setController(null);
        //此处设置为null，就不会再java下创建xml的文件夹了
        templateConfig.setXml(null);
        templateConfig.setEntity(null);
        templateConfig.setService(null);
        templateConfig.setServiceImpl(null);
        templateConfig.setMapper(null);
        templateConfig.setEntityKt(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setEntityTableFieldAnnotationEnable(true);
        strategy.setRestControllerStyle(true);
        // 公共父类
        //strategy.setSuperControllerClass("com.baomidou.ant.common.BaseController");
        // 写于父类中的公共字段
        //strategy.setSuperEntityColumns("id");
        //表名
        strategy.setInclude("ren_hai_common_module");
        strategy.setControllerMappingHyphenStyle(true);
        //根据你的表名来建对应的类名，如果你的表名没有什么下划线，比如test，那么你就可以取消这一步
        // strategy.setTablePrefix("t_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }

}