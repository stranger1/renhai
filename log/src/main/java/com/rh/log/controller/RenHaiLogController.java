package com.rh.log.controller;


import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.rh.entity.log.RenHaiLog;
import com.rh.log.service.RenHaiLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.Page;
import renhai.util.ResponseContext;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wangdao
 * @since 2020-04-21
 */
@Api("日志")
@RestController
@RequestMapping("/api/log/")
public class RenHaiLogController {


    private final RenHaiLogService renHaiLogService;

    @Autowired
    public RenHaiLogController(RenHaiLogService renHaiLogService) {
        this.renHaiLogService = renHaiLogService;
    }

    /**
     * 查询分页数据
     */
    @ApiOperation(value = "查询分页数据")
    @RequestMapping(value = "/getList")
    public ResponseContext<PageInfo<RenHaiLog>> getList(@RequestBody Page<RenHaiLog> page) {

        return new ResponseContext<>(renHaiLogService.list(page));
    }


    /**
     * 根据id查询
     */
    @ApiOperation(value = "根据id查询数据")
    @RequestMapping(value = "/getById")
    public ResponseContext<RenHaiLog> getById(@RequestParam("id") Serializable id) {
        return new ResponseContext<>(renHaiLogService.getById(id));
    }

    /**
     * 新增
     */
    @ApiOperation(value = "新增/修改 数据")
    @PostMapping(value = "/saveUpdate")
    public ResponseContext<Long> saveUpdate(@RequestBody RenHaiLog renHaiLog) {
        if(renHaiLog.getLogId()!=null){
            System.out.println("修改");
            //renHaiLogService.updateById(renHaiLog);
        }else{
            System.out.println("添加");
            renHaiLogService.save(renHaiLog);
        }
        return new ResponseContext<>(renHaiLog.getLogId());
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除数据")
    @RequestMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestParam("id") Serializable id) {
        return new ResponseContext<>(renHaiLogService.removeById(id));
    }
}
