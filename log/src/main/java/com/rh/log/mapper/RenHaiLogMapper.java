package com.rh.log.mapper;

import com.rh.entity.log.RenHaiLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
* <p>
    *  Mapper 接口
    * </p>
*
* @author wangdao
* @since 2020-04-21
*/
@Mapper
@Repository
public interface RenHaiLogMapper extends BaseMapper<RenHaiLog> {

}
