package com.rh.log.service.impl;

import com.rh.entity.log.RenHaiLog;
import com.rh.log.mapper.RenHaiLogMapper;
import com.rh.log.service.RenHaiLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-04-21
 */
@Service
public class RenHaiLogServiceImpl extends ServiceImpl<RenHaiLogMapper, RenHaiLog> implements RenHaiLogService {

   private final RenHaiLogMapper renHaiLogMapper;

   private final RestTemplateUtil restTemplateUtil;

   public  RenHaiLogServiceImpl(RenHaiLogMapper renHaiLogMapper, RestTemplateUtil restTemplateUtil) {
     this.renHaiLogMapper = renHaiLogMapper;
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<RenHaiLog> list(Page page) {
     QueryWrapper<RenHaiLog> queryWrapper = new QueryWrapper<>();
       queryWrapper.lambda().orderByDesc(RenHaiLog::getLogOperationTime);
     PageHelper.startPage(page);
     return new PageInfo<>(renHaiLogMapper.selectList(queryWrapper));
   }
}
