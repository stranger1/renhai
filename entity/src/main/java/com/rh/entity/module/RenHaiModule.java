package com.rh.entity.module;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Data
public class RenHaiModule implements Serializable {



    @TableId
    private String moduleId;
    private String moduleName;
    private String moduleUrl;
    /**
     * 模块等级(1目录，2菜单)
     */
    private String moduleGrade;
    private String moduleAuthority;
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date updateTime;

    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Integer status;

    private String modulePid;

    @TableField(exist = false)
    public List<RenHaiModule> children = new ArrayList<>();

    @Override
    public String toString() {
        return "RenHaiModule{" +
                ", moduleId=" + moduleId +
                ", moduleName=" + moduleName +
                ", moduleUrl=" + moduleUrl +
                ", moduleGrade=" + moduleGrade +
                ", moduleAuthority=" + moduleAuthority +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", status=" + status +
                ", modulePid=" + modulePid +
                "}";
    }
}
