package com.rh.entity.file;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ApiModel("文件分片详情")
public class RenHaiFileDetails {
    @ApiModelProperty(name = "fileDetailsId", value = "文件详情id")
    @TableId
    private String fileDetailsId;
    @ApiModelProperty(name = "renHaiFile", value = "文件id")
    @TableField(el = "renHaiFile.fileId,jdbcType=VARCHAR")
    private RenHaiFile renHaiFile;
    @ApiModelProperty(name = "chunkNumber", value = "当前块")
    private Long chunkNumber;
    @ApiModelProperty(name = "totalChunks", value = "文件被分成的块数")
    private Integer totalChunks;
    @ApiModelProperty(name = "totalSize", value = "文件总大小")
    private Long totalSize;
    @ApiModelProperty(name = "currentChunkSize", value = "当前快的实际大小")
    private Integer currentChunkSize;
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(name = "status", value = "状态/")
    private Integer status;
    /**
     * 页面写入数据库时格式化
     */
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date createTime;
    /**
     * 页面写入数据库时格式化
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date updateTime;

}
