package com.rh.result.module;

import com.rh.entity.module.RenHaiModule;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author qingmu
 * @since 2019-10-28
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RenHaiModuleResult extends RenHaiModule {

    private RenHaiModule pModule;

}
