package com.rh.redis.redieserver;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import renhai.util.redis.RedisUtil;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author 57556
 */
@Service
public class RedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 设置set
     *
     * @param key   键
     * @param value 值
     * @param <T>   泛型
     * @return true
     */
    public <T> boolean set(String key, T value, Integer... time) {
        try {
            //任意类型转换成String
            String val = RedisUtil.beanToString(value);
            if (val == null || val.length() <= 0) {
                return false;
            }
            if (time != null && time.length > 0) {
                stringRedisTemplate.opsForValue().set(key, val, time[0], TimeUnit.MILLISECONDS);
            } else {
                stringRedisTemplate.opsForValue().set(key, val);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * set 通过键获取值
     *
     * @param key   键
     * @param clazz 类型
     * @param <T>   泛型
     * @return 值
     */
    public <T> T get(String key, Class<T> clazz) {
        try {
            String value = stringRedisTemplate.opsForValue().get(key);
            return RedisUtil.stringToBean(value, clazz);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 删除
     *
     * @param key
     */
    public void remove(String key) {
        stringRedisTemplate.delete(key);
    }

    /**
     * 清空缓存
     */
    public void clear() {
        Set<String> keys = stringRedisTemplate.keys("*");
        if (!CollectionUtils.isEmpty(keys)) {
            stringRedisTemplate.delete(keys);
        }
    }

    public Integer getSize() {
        Long size = stringRedisTemplate.execute((RedisCallback<Long>) RedisServerCommands::dbSize);
        return size.intValue();
    }

    /**
     * 根据key获取过期时间并换算成指定单位
     *
     * @param key
     * @return
     */
    public Long getExpire(String key) {
        return stringRedisTemplate.getExpire(key, TimeUnit.MILLISECONDS);
    }

    /**
     * 检查key是否存在，返回boolean值
     *
     * @param key
     * @return
     */
    public boolean hasKey(String key) {

        return stringRedisTemplate.hasKey(key);
    }

    /**
     * 设置过期时间
     *
     * @param key
     * @param time
     */
    public void expire(String key, Integer time) {
        //设置过期时间
        stringRedisTemplate.expire(key, time, TimeUnit.MILLISECONDS);
    }



/*    //向redis里存入数据和设置缓存时间
stringRedisTemplate.opsForValue().set("baike", "100", 60 * 10, TimeUnit.SECONDS);
//val做-1操作
stringRedisTemplate.boundValueOps("baike").increment(-1);
//根据key获取缓存中的val
stringRedisTemplate.opsForValue().get("baike")
//val +1
stringRedisTemplate.boundValueOps("baike").increment(1);
//根据key删除缓存
stringRedisTemplate.delete("baike");
//检查key是否存在，返回boolean值
stringRedisTemplate.hasKey("baike");
//向指定key中存放set集合
stringRedisTemplate.opsForSet().add("baike", "1","2","3");
//设置过期时间
stringRedisTemplate.expire("baike",1000 , TimeUnit.MILLISECONDS);
//根据key查看集合中是否存在指定数据
stringRedisTemplate.opsForSet().isMember("baike", "1");
//根据key获取set集合
stringRedisTemplate.opsForSet().members("baike");
    //验证有效时间
    Long expire = redisTemplate.boundHashOps("baike").getExpire();
System.out.println("redis有效时间："+expire+"S");*/


}