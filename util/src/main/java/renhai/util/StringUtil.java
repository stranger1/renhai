package renhai.util;

import java.util.List;
import java.util.UUID;

public class StringUtil {

    public static String getSuuId() {
        //注意replaceAll前面的是正则表达式
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
    public static Long getLuuId() {
        //注意replaceAll前面的是正则表达式
        return Long.parseLong(UUID.randomUUID().toString().replaceAll("-", ""));
    }

    public static <T> boolean checkList(List<T> list){
        return list!=null && !list.isEmpty();
    }

}
