package renhai.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 57556
 */
public class DataUtil {
    /**
     * 获取当前时间
     *
     * @return
     */
    public static Long getDataTime() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String time = now.format(f);
        return Long.parseLong(time);
    }

    public static Long getData() {
        LocalDate now = LocalDate.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyyMMdd");
        String time = now.format(f);
        return Long.parseLong(time);
    }

    public static String getData(String path) {
        LocalDate now = LocalDate.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy" + path + "MM" + path + "dd");
        return now.format(f);
    }

    public static Long getTime() {
        LocalTime now = LocalTime.now();
        DateTimeFormatter f = DateTimeFormatter.ofPattern("HHmmss");
        String time = now.format(f);
        return Long.parseLong(time);
    }

    public static void main(String[] args) {
        System.out.println(DataUtil.getData("/"));
    }


}
