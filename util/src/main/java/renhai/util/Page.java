package renhai.util;

import lombok.Data;

@Data
public class Page<T> {
    //当前页
    private int pageNum;
    //每页的数量
    private int pageSize;
    //参数
    private T params;

}
